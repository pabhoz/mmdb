class Image extends MultimediaElement{

    constructor(file, type = null){
        super(file, type, document.createElement("img"));
    }

}