var dropArea = new DropArea("#dropArea");
var fileCounter = 0;

function defaultBehavior(el){
    let da = document.querySelector("#dropArea");

    let ct = da.querySelector("#dropArea .centeredTitle");
    ct.innerHTML = "Current Files:";
    ct.style.borderBottom = "1px dashed gray";
    ct.style.marginBottom = "5px";
    ct.style.justifyContent = "flex-start";
    da.classList.add("multimedia");

    fileCounter++;

    da.innerHTML += `
    <div class="file" id="file${fileCounter}">
        <span class="name">${el.file.name}</span> 
        <div class="info">
            <span class="type">${el.type}</span> 
            <span class="size">${btoMB(el.file.size)}MB</span>
        </div>
    </div>`;

    return da;
}

function btoMB(b){
    return Math.round( (((b/8)/1024)/1024) * 100 ) / 100;
}

let imageBehavior = (file) => {
    var img = new Image(file);
    img.DOMElement.classList.add("thumb");
    img.loadFileContent().then(()=>{
        let da = defaultBehavior(img);
        da.querySelector(`#file${fileCounter} .info`).prepend(img.DOMElement);
    });
};

let audioBehavior = (file) => {
    var audio = new Audio(file);
    audio.DOMElement.controls = true;
    audio.loadFileContent().then(()=>{
        let da = defaultBehavior(audio);
        da.querySelector(`#file${fileCounter} .info`).prepend(audio.DOMElement);
    });
};

dropArea.subscribe("image", imageBehavior);
dropArea.subscribe("audio", audioBehavior);

